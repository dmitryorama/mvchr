﻿using System;
using MvcHR.DataModelDB;

namespace MvcHR.DAL
{
    public class UnitOfWork : IDisposable
    {
        private HRDatabaseEntities context;
        private GenericRepository<Aspirant> aspirantRepository;
        private GenericRepository<Company> companyRepository;
        private GenericRepository<Event> eventRepository;
        private GenericRepository<Reminder> reminderRepository;

        public UnitOfWork()
        {
            context = new HRDatabaseEntities();
        }

        public UnitOfWork(string nameOrConnectionString)
        {
            context = new HRDatabaseEntities(nameOrConnectionString);
        }

        public GenericRepository<Aspirant> AspirantRepository
        {
            get
            {

                if (this.aspirantRepository == null)
                {
                    this.aspirantRepository = new GenericRepository<Aspirant>(context);
                }
                return aspirantRepository;
            }
        }

        public GenericRepository<Company> CompanyRepository
        {
            get
            {

                if (this.companyRepository == null)
                {
                    this.companyRepository = new GenericRepository<Company>(context);
                }
                return companyRepository;
            }
        }

        public GenericRepository<Event> EventRepository { 
            get 
            {
                if (this.eventRepository == null)
                {
                    this.eventRepository = new GenericRepository<Event>(context);
                }
                return eventRepository;
            } 
        }

        public GenericRepository<Reminder> ReminderRepository {
            get 
            {
                if (this.reminderRepository == null)
                {
                    this.reminderRepository = new GenericRepository<Reminder>(context);
                }
                return reminderRepository;
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}