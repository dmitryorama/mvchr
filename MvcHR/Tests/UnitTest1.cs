﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Xunit;
using MvcHR.DataModelDB;
using MvcHR.DAL;

namespace MvcHR.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void CreateUnitOfWorkTest()
        {
            using (var uow = new UnitOfWork("name=HRTestEntities"))
            {
                Assert.NotNull(uow);            
            }
        }

        [Fact]
        public void InsertTest()
        {
            using (var uow = new UnitOfWork("name=HRTestEntities"))
            {
                //Aspirant
                Assert.Throws<ArgumentNullException>(() => uow.AspirantRepository.Insert(null));
                Aspirant aspirant1 = new Aspirant() { FirstName = "John", LastName = "Doe", Age = 23, EducationID = 4, Comment = "Test User", Contact = "Call 9-1-1" };
                Assert.DoesNotThrow(() => uow.AspirantRepository.Insert(aspirant1));

                //Company
                Assert.Throws<ArgumentNullException>(() => uow.CompanyRepository.Insert(null));
                Company company1 = new Company() { CompanyName = "Рога и Копыта", Contact = "ул. Разбитых фонарей, д.1. Тел.: 102", Comment = "No comments" };
                Assert.DoesNotThrow(() => uow.CompanyRepository.Insert(company1));

                //Event
                Assert.Throws<ArgumentNullException>(() => uow.EventRepository.Insert(null));
                Event event1 = new Event() { EventTime = DateTime.Now, StatusID = 1, AspirantID = aspirant1.AspirantID, CompanyID = company1.CompanyID, Comment = "Test event" };
                Assert.DoesNotThrow(() => uow.EventRepository.Insert(event1));

                //Reminder
                Assert.Throws<ArgumentNullException>(() => uow.ReminderRepository.Insert(null));
                Reminder reminder1 = new Reminder() { EventID = event1.EventID, ReminderTime = DateTime.Now };
                Assert.DoesNotThrow(() => uow.ReminderRepository.Insert(reminder1));

                //Assert.DoesNotThrow(() => uow.Save());
            }
        }

        [Fact]
        public void GetTest()
        {
            using (var uow = new UnitOfWork("name=HRTestEntities"))
            {
                //Aspirant
                Assert.NotNull(uow.AspirantRepository.GetByID(2));
                var resA1 = uow.AspirantRepository.Get().ToList();
                Assert.NotNull(resA1);

                string qA1 = "SELECT * FROM Aspirants WHERE AspirantID = @p0";
                Assert.NotNull(uow.AspirantRepository.GetWithRawSql(qA1, 2));

                //Company
                Assert.NotNull(uow.CompanyRepository.GetByID(1));
                var resC1 = uow.CompanyRepository.Get().ToList();
                Assert.NotNull(resC1);

                string qC1 = "SELECT * FROM Companies WHERE CompanyID = @p0";
                Assert.NotNull(uow.CompanyRepository.GetWithRawSql(qC1, 1));

                //Event
                Assert.NotNull(uow.EventRepository.GetByID(1));
                var resE1 = uow.EventRepository.Get().ToList();
                Assert.NotNull(resE1);

                string qE1 = "SELECT * FROM Events WHERE EventID = @p0";
                Assert.NotNull(uow.EventRepository.GetWithRawSql(qE1, 1));

                //Reminder
                Assert.NotNull(uow.ReminderRepository.GetByID(1));
                var resR1 = uow.ReminderRepository.Get().ToList();
                Assert.NotNull(resR1);

                string qR1 = "SELECT * FROM Reminders WHERE ReminderID = @p0";
                Assert.NotNull(uow.ReminderRepository.GetWithRawSql(qR1, 1));
            }
        }

        [Fact]
        public void FilterTest()
        {
            using (var uow = new UnitOfWork("name=HRTestEntities"))
            {
                //Aspirant
                var resA1 = uow.AspirantRepository.Get(a => a.FirstName == "John" && a.Age == 23, q => q.OrderBy(s => s.LastName));
                Assert.NotNull(resA1.ToList());

                //Company
                var resC1 = uow.CompanyRepository.Get(c => c.CompanyName.Contains("ога"), q => q.OrderBy(s => s.CompanyName));
                Assert.NotNull(resC1.ToList());

                //Event
                var resE1 = uow.EventRepository.Get(e => e.StatusID == 1, q => q.OrderBy(s => s.EventTime));
                Assert.NotNull(resE1.ToList());

                //Reminder
                var resR1 = uow.ReminderRepository.Get(r => r.ReminderTime <= DateTime.Now, q => q.OrderBy(s => s.ReminderTime));
                Assert.NotNull(resR1.ToList());
            }
        }

        [Fact]
        public void PagingTest()
        {
            using (var uow = new UnitOfWork("name=HRTestEntities"))
            {
                int pageCount;
                //Aspirant
                var resA1 = uow.AspirantRepository.GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.LastName));
                Assert.NotNull(resA1);

                //Company
                var resC1 = uow.CompanyRepository.GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.CompanyName));
                Assert.NotNull(resC1);

                //Event
                var resE1 = uow.EventRepository.GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.EventTime));
                Assert.NotNull(resE1);

                //Reminder
                var resR1 = uow.ReminderRepository.GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.ReminderTime));
                Assert.NotNull(resR1);
            }
        }

        [Fact]
        public void PagingFilteredTest()
        {
            using (var uow = new UnitOfWork("name=HRTestEntities"))
            {
                int pageCount;
                //Aspirant
                var resA1 = uow.AspirantRepository.GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.LastName), a => a.Age > 10);
                Assert.NotNull(resA1);

                //Company
                var resC1 = uow.CompanyRepository.
                    GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.CompanyName), c => c.CompanyName.Contains("ога"));
                Assert.NotNull(resC1);

                //Event
                var resE1 = uow.EventRepository.
                    GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.EventTime), e => e.EventTime < DateTime.Now);
                Assert.NotNull(resE1);

                //Reminder
                var resR1 = uow.ReminderRepository.
                    GetPage(1, 3, out pageCount, q => q.OrderBy(s => s.ReminderTime), r => r.ReminderTime <= DateTime.Now);
                Assert.NotNull(resR1);
            }
        }

        [Fact]
        public void UpdateTest()
        {
            using (var uow = new UnitOfWork("name=HRTestEntities"))
            {
                //Aspirant
                Aspirant a1 = uow.AspirantRepository.GetByID(2);
                a1.Age++;
                Assert.DoesNotThrow(() => uow.AspirantRepository.Update(a1));

                //Company
                Company c1 = uow.CompanyRepository.GetByID(1);
                c1.Comment = "Так себе компания";
                Assert.DoesNotThrow(() => uow.CompanyRepository.Update(c1));

                //Event
                Event e1 = uow.EventRepository.GetByID(1);
                e1.StatusID = 2;
                Assert.DoesNotThrow(() => uow.EventRepository.Update(e1));

                //Reminder
                Reminder r1 = uow.ReminderRepository.GetByID(1);
                r1.ReminderTime = DateTime.Now.AddMonths(1);
                Assert.DoesNotThrow(() => uow.ReminderRepository.Update(r1));

                //Assert.DoesNotThrow(() => uow.Save());
            }
        }
    }
}
